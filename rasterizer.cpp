// clang-format off
//
// Created by goksu on 4/6/19.
//

#include <algorithm>
#include <vector>
#include "rasterizer.hpp"
#include <opencv2/opencv.hpp>
#include <math.h>


rst::pos_buf_id rst::rasterizer::load_positions(const std::vector<Eigen::Vector3f> &positions)
{
    auto id = get_next_id();
    pos_buf.emplace(id, positions);

    return {id};
}

rst::ind_buf_id rst::rasterizer::load_indices(const std::vector<Eigen::Vector3i> &indices)
{
    auto id = get_next_id();
    ind_buf.emplace(id, indices);

    return {id};
}

rst::col_buf_id rst::rasterizer::load_colors(const std::vector<Eigen::Vector3f> &cols)
{
    auto id = get_next_id();
    col_buf.emplace(id, cols);

    return {id};
}

auto to_vec4(const Eigen::Vector3f& v3, float w = 1.0f)
{
    return Vector4f(v3.x(), v3.y(), v3.z(), w);
}


static bool insideTriangle(int x, int y, const Vector3f* _v)
{   
    // TODO : Implement this function to check if the point (x, y) is inside the triangle represented by _v[0], _v[1], _v[2]
}

static std::tuple<float, float, float> computeBarycentric2D(float x, float y, const Vector3f* v)
{
    float c1 = (x*(v[1].y() - v[2].y()) + (v[2].x() - v[1].x())*y + v[1].x()*v[2].y() - v[2].x()*v[1].y()) / (v[0].x()*(v[1].y() - v[2].y()) + (v[2].x() - v[1].x())*v[0].y() + v[1].x()*v[2].y() - v[2].x()*v[1].y());
    float c2 = (x*(v[2].y() - v[0].y()) + (v[0].x() - v[2].x())*y + v[2].x()*v[0].y() - v[0].x()*v[2].y()) / (v[1].x()*(v[2].y() - v[0].y()) + (v[0].x() - v[2].x())*v[1].y() + v[2].x()*v[0].y() - v[0].x()*v[2].y());
    float c3 = (x*(v[0].y() - v[1].y()) + (v[1].x() - v[0].x())*y + v[0].x()*v[1].y() - v[1].x()*v[0].y()) / (v[2].x()*(v[0].y() - v[1].y()) + (v[1].x() - v[0].x())*v[2].y() + v[0].x()*v[1].y() - v[1].x()*v[0].y());
    return {c1,c2,c3};
}

void rst::rasterizer::draw(pos_buf_id pos_buffer, ind_buf_id ind_buffer, col_buf_id col_buffer, Primitive type)
{
    //为什么这里要用map，不是多此一举吗？
    auto& buf = pos_buf[pos_buffer.pos_id];
    auto& ind = ind_buf[ind_buffer.ind_id];
    auto& col = col_buf[col_buffer.col_id];

    float f1 = (50 - 0.1) / 2.0;
    float f2 = (50 + 0.1) / 2.0;

    Eigen::Matrix4f mvp = projection * view * model;
    for (auto& i : ind)
    {
        Triangle t;
        Eigen::Vector4f v[] = {
                mvp * to_vec4(buf[i[0]], 1.0f),
                mvp * to_vec4(buf[i[1]], 1.0f),
                mvp * to_vec4(buf[i[2]], 1.0f)
        };
        t.rhw[0] = 1.0f / v[0].w();
        t.rhw[1] = 1.0f / v[1].w();
        t.rhw[2] = 1.0f / v[2].w();
        //Homogeneous division
        for (auto& vec : v) {
            vec /= vec.w();
        }
        //Viewport transformation
        for (auto & vert : v)
        {
            vert.x() = 0.5*width*(vert.x()+1.0);
            vert.y() = 0.5*height*(vert.y()+1.0);
            vert.z() = vert.z() * f1 + f2;  //这是什么意思 ?????
        }

        for (int i = 0; i < 3; ++i)
        {
            //为什么要重复三次，好像没什么用
            t.setVertex(i, v[i].head<3>());
            //t.setVertex(i, v[i].head<3>());
            //t.setVertex(i, v[i].head<3>());
        }

        auto col_x = col[i[0]];
        auto col_y = col[i[1]];
        auto col_z = col[i[2]];

        t.setColor(0, col_x[0], col_x[1], col_x[2]);
        t.setColor(1, col_y[0], col_y[1], col_y[2]);
        t.setColor(2, col_z[0], col_z[1], col_z[2]);

        rasterize_triangle(t);
    }
}
float rst::rasterizer::Between(float min, float max, float num) {
    if (num >= min && num <= max)return num;
    if (num < min)return min;
    if (num > max)return max;
}

//Screen space rasterization
//void rst::rasterizer::rasterize_triangle(const Triangle& t) {
//    auto v = t.toVector2();
//    
//    //计算bounding box
//    int min_x, max_x, min_y, max_y;
//    min_x = max_x = t.v[0].x();
//    min_y = max_y = t.v[0].y();
//    for (int i = 1; i < 3; ++i) {
//        min_x = Between(0, width - 1, std::min((int)t.v[i].x(), min_x));
//        max_x = Between(0, width - 1, std::max((int)t.v[i].x(), max_x));
//        min_y = Between(0, height - 1, std::min((int)t.v[i].y(), min_y));
//        max_y = Between(0, height - 1, std::max((int)t.v[i].y(), max_y));
//    }
//    for (int y = min_y; y <= max_y; ++y) {
//        for (int x = min_x; x <= max_x; ++x) {
//            Vector2f pos(x + 0.5, y + 0.5);
//            float E0 = (pos.x() - v[0].x()) * (v[1].y() - v[0].y()) - (pos.y() - v[0].y()) * (v[1].x() - v[0].x());
//            float E1 = (pos.x() - v[1].x()) * (v[2].y() - v[1].y()) - (pos.y() - v[1].y()) * (v[2].x() - v[1].x());
//            float E2 = (pos.x() - v[2].x()) * (v[0].y() - v[2].y()) - (pos.y() - v[2].y()) * (v[0].x() - v[2].x());
//            if (!((E0 >= 0 && E1 >= 0 && E2 >= 0) || (E0 <= 0 && E1 <= 0 && E2 <= 0))) {
//                continue;
//            }
//            Vector2f S0 = v[0] - pos;
//            Vector2f S1 = v[1] - pos;
//            Vector2f S2 = v[2] - pos;
//
//            float a = std::abs((S1.x() * S2.y()) - (S1.y() * S2.x()));
//            float b = std::abs((S0.x() * S2.y()) - (S0.y() * S2.x()));
//            float c = std::abs((S0.x() * S1.y()) - (S0.y() * S1.x()));
//
//            float s = a + b + c;
//
//            a /= s; b /= s; c /= s;
//
//            float rhw = a * t.rhw[0] + b * t.rhw[1] + c * t.rhw[2];
//            float w = 1.0f / rhw;
//            auto ind = (height - 1 - y) * width + x;
//            if (w > depth_buf[ind])
//                continue;
//            else depth_buf[ind] = w;
//
//            a = a * w * t.rhw[0];
//            b = b * w * t.rhw[1];
//            c = c * w * t.rhw[2];
//
//            Vector3f color = a * t.color[0] + b * t.color[1] + c * t.color[2];
//
//            Vector3f point(x, y, 10);
//            set_pixel(point, color);
//        }
//    }
//}

void rst::rasterizer::rasterize_triangle(const Triangle& t) {
    auto v = t.toVector2();

    //计算bounding box
    int min_x, max_x, min_y, max_y;
    min_x = max_x = t.v[0].x();
    min_y = max_y = t.v[0].y();
    int num = 0;
    for (int i = 1; i < 3; ++i) {
        min_x = Between(0, width - 1, std::min((int)t.v[i].x(), min_x));
        max_x = Between(0, width - 1, std::max((int)t.v[i].x(), max_x));
        min_y = Between(0, height - 1, std::min((int)t.v[i].y(), min_y));
        max_y = Between(0, height - 1, std::max((int)t.v[i].y(), max_y));
    }
    for (int y = min_y; y <= max_y; ++y) {
        for (int x = min_x; x <= max_x; ++x) {
            //Vector2f pos(x + 0.5, y + 0.5);
            //float E0 = (pos.x() - v[0].x()) * (v[1].y() - v[0].y()) - (pos.y() - v[0].y()) * (v[1].x() - v[0].x());
            //float E1 = (pos.x() - v[1].x()) * (v[2].y() - v[1].y()) - (pos.y() - v[1].y()) * (v[2].x() - v[1].x());
            //float E2 = (pos.x() - v[2].x()) * (v[0].y() - v[2].y()) - (pos.y() - v[2].y()) * (v[0].x() - v[2].x());
            //if (!((E0 >= 0 && E1 >= 0 && E2 >= 0) || (E0 <= 0 && E1 <= 0 && E2 <= 0))) {
            //    continue;
            //}
            //Vector2f S0 = v[0] - pos;
            //Vector2f S1 = v[1] - pos;
            //Vector2f S2 = v[2] - pos;

            //float a = std::abs((S1.x() * S2.y()) - (S1.y() * S2.x()));
            //float b = std::abs((S0.x() * S2.y()) - (S0.y() * S2.x()));
            //float c = std::abs((S0.x() * S1.y()) - (S0.y() * S1.x()));

            //float s = a + b + c;
            //if (s == 0)continue;
            //a /= s; b /= s; c /= s;
            //float rhw = a * t.rhw[0] + b * t.rhw[1] + c * t.rhw[2];
            //float w = 1.0f / rhw;
            //a = a * w * t.rhw[0];
            //b = b * w * t.rhw[1];
            //c = c * w * t.rhw[2];
            //Vector3f color = a * t.color[0] + b * t.color[1] + c * t.color[2];
            int first = 0;
            Vector3f color;
            for (float sy = y; sy < y + 1; sy += 0.25f) {
                for (float sx = x; sx < x + 1; sx += 0.25f) {
                    Vector2f spos(sx + 0.125f, sy + 0.125f);
                    float E0 = (spos.x() - v[0].x()) * (v[1].y() - v[0].y()) - (spos.y() - v[0].y()) * (v[1].x() - v[0].x());
                    float E1 = (spos.x() - v[1].x()) * (v[2].y() - v[1].y()) - (spos.y() - v[1].y()) * (v[2].x() - v[1].x());
                    float E2 = (spos.x() - v[2].x()) * (v[0].y() - v[2].y()) - (spos.y() - v[2].y()) * (v[0].x() - v[2].x());
                    if (!((E0 >= 0 && E1 >= 0 && E2 >= 0) || (E0 <= 0 && E1 <= 0 && E2 <= 0))) {
                        continue;
                    }
                    Vector2f S0 = v[0] - spos;
                    Vector2f S1 = v[1] - spos;
                    Vector2f S2 = v[2] - spos;
                    float a = std::abs((S1.x() * S2.y()) - (S1.y() * S2.x()));
                    float b = std::abs((S0.x() * S2.y()) - (S0.y() * S2.x()));
                    float c = std::abs((S0.x() * S1.y()) - (S0.y() * S1.x()));
                    float s = a + b + c;

                    if (s == 0.0f)continue;

                    a /= s; b /= s; c /= s;
                    float rhw = a * t.rhw[0] + b * t.rhw[1] + c * t.rhw[2];
                    float w = 1.0f / rhw;
                    //std::cout << a << " " << b << " " << c << " " <<rhw << std::endl;
                    //std::cout << w << std::endl;
                    auto ind = (height - 1 - y) * width + x;
                    if (w < depth_buf1[ind]((int)((sy - y) / 0.25f), (int)((sx - x) / 0.25f))) {
                        continue;
                    }
                    else {
                        depth_buf1[ind]((int)((sy - y) / 0.25f), (int)((sx - x) / 0.25f)) = w;
                        if (first==0) {
                            color = a * t.color[0] + b * t.color[1] + c * t.color[2];
                            first++;
                        }
                        num++;
                    }
                }
            }
            color = color * ((float)num / 16.0f);
            num = 0;
            Vector3f point(x, y, 10);
            set_pixel(point, color);
        }
    }
}

void rst::rasterizer::set_model(const Eigen::Matrix4f& m)
{
    model = m;
}

void rst::rasterizer::set_view(const Eigen::Matrix4f& v)
{
    view = v;
}

void rst::rasterizer::set_projection(const Eigen::Matrix4f& p)
{
    projection = p;
}

void rst::rasterizer::clear(rst::Buffers buff)
{
    if ((buff & rst::Buffers::Color) == rst::Buffers::Color)
    {
        std::fill(frame_buf.begin(), frame_buf.end(), Eigen::Vector3f{0, 0, 0});
    }
    if ((buff & rst::Buffers::Depth) == rst::Buffers::Depth)
    {
        float number = -1000.0f;
        Matrix4f temp;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                temp(i, j) = number;
            }
        }
        for (int i = 0; i < width * height;++i) {
            depth_buf1[i] = temp;
        }
    }
}

rst::rasterizer::rasterizer(int w, int h) : width(w), height(h)
{
    frame_buf.resize(w * h);
    depth_buf1.resize(w * h);
}

int rst::rasterizer::get_index(int x, int y)
{
    return (height-1-y)*width + x;
}

void rst::rasterizer::set_pixel(const Eigen::Vector3f& point, const Eigen::Vector3f& color)
{
    //old index: auto ind = point.y() + point.x() * width;
    auto ind = (height-1-point.y())*width + point.x();
    frame_buf[ind] += color;

}

// clang-format on